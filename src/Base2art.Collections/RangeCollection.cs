﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public class RangeCollection<T> : IEnumerable<T> where T : IComparable<T>
    {
        private readonly T start;

        private readonly T stop;

        private readonly T step;

        private readonly Func<T, T, T> addTwoNumbers;
        
        public RangeCollection(T start, T stop, T step, Func<T, T, T> addTwoNumbers)
        {
            addTwoNumbers.ValidateIsNotNull();
            
            if (step.CompareTo(default(T)) == 0)
            {
                throw new ArgumentOutOfRangeException("step", step, "Value must not be '0'");
            }
            
            this.addTwoNumbers = addTwoNumbers;
            this.step = step;
            this.stop = stop;
            this.start = start;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var temp = this.start;
            while (
                this.step.CompareTo(default(T)) > 0 
                ? temp.CompareTo(this.stop) < 0 
                : temp.CompareTo(this.stop) > 0)
            {
                var t = temp;
                temp = this.addTwoNumbers(temp, this.step);
                yield return t;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
