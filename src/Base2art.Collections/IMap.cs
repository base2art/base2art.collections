﻿namespace Base2art.Collections
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public interface IMap<TKey, TValue> : IReadOnlyMap<TKey, TValue>, IMapping<TKey, TValue>
    {
        new TValue this[TKey key] { get; set; }
    }
}