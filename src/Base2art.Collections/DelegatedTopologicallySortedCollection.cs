﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DelegatedTopologicallySortedCollection<TKey, TItem> : TopologicallySortedCollection<TKey, TItem> where TKey : IComparable<TKey> where TItem : class
    {
        private readonly Func<TItem, IEnumerable<TKey>> refsFunc;

        private readonly Func<TItem, TKey> keyFunc;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "SjY")]
        public DelegatedTopologicallySortedCollection(Func<TItem, TKey> keyFunc, Func<TItem, IEnumerable<TKey>> refsFunc)
        {
            keyFunc.ValidateIsNotNull();
            refsFunc.ValidateIsNotNull();
                        
            this.keyFunc = keyFunc;
            this.refsFunc = refsFunc;
        }

        protected override TKey GetKey(TItem item)
        {
            return this.keyFunc(item);
        }

        protected override IEnumerable<TKey> GetDependencies(TItem item)
        {
            return this.refsFunc(item);
        }
    }
}
