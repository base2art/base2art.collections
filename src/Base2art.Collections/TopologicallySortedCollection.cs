﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public abstract class TopologicallySortedCollection<TKey, TItem>
        : ICollection<TItem>
        where TKey : IComparable<TKey>
        where TItem : class
    {
        private readonly LinkedList<Wrapper> items = new LinkedList<Wrapper>();
        
        public int Count
        {
            get { return this.items.Count; }
        }
        
        public bool IsReadOnly
        {
            get { return false; }
        }
        
        public void Add(TItem item)
        {
            // var key = this.GetKey(item);
            var wrapper = new Wrapper(this, item);
            
            this.items.AddLast(wrapper);
            
            this.Refresh(wrapper);
            this.UpdateMain();
        }

        public void Clear()
        {
            this.items.Clear();
        }
        
        public bool Contains(TItem item)
        {
            return this.items.Any(x => x.Key.CompareTo(this.GetKey(item)) == 0);
        }
        
        public void CopyTo(TItem[] array, int arrayIndex)
        {
            this.items.Select(x => x.Item).ToArray().CopyTo(array, arrayIndex);
        }
        
        public bool Remove(TItem item)
        {
            var key = this.GetKey(item);
            foreach (var other in this.items)
            {
                var inrefs = other.InRefs;
                for (var i = inrefs.Count - 1; i >= 0; i--)
                {
                    var inref = inrefs[i];
                    if (inref.Key.CompareTo(key) == 0)
                    {
                        inrefs.RemoveAt(i);
                    }
                }
            }
            
            var curr = this.items.First;
            while (curr != null)
            {
                if (curr.Value.Key.CompareTo(this.GetKey(item)) == 0)
                {
                    this.items.Remove(curr);
                    this.UpdateMain();
                    return true;
                }
                
                curr = curr.Next;
            }
            
            this.UpdateMain();
            return false;
        }
        
        public IEnumerator<TItem> GetEnumerator()
        {
            return this.items.Select(x => x.Item).GetEnumerator();
        }
        
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
        
        public TItem[] Resolve()
        {
            return this.ToArray();
        }
        
        protected abstract TKey GetKey(TItem item);
        
        protected TKey[] GetDependenciesSafe(TItem item)
        {
            return (this.GetDependencies(item) ?? new TKey[0]).ToArray();
        }
        
        protected abstract IEnumerable<TKey> GetDependencies(TItem item);

        private void UpdateMain()
        {
            var temp = new LinkedList<Wrapper>(this.items);
            this.items.Clear();
            foreach (var item in temp)
            {
                item.InRefsForMarking.Clear();
                item.InRefsForMarking.AddRange(item.InRefs);
            }
            
            this.AddToQueue(temp, this.items);
        }

        private void AddToQueue(LinkedList<Wrapper> input, LinkedList<Wrapper> output)
        {
            if (input.Count == 0)
            {
                return;
            }
            
            var first = input.FirstOrDefault(x => x.InRefsForMarking.Count == 0);
            if (first == null)
            {
                throw new InvalidOperationException("Circular reference (probably)");
            }
            
            input.Remove(first);
            output.AddFirst(first);
            foreach (var item in input)
            {
                if (item.InRefsForMarking.Contains(first))
                {
                    item.InRefsForMarking.Remove(first);
                }
            }
            
            this.AddToQueue(input, output);
        }

        private void Refresh(Wrapper wrapper)
        {
            foreach (var other in this.items)
            {
                if (Array.Exists(wrapper.Requires, x => x.CompareTo(other.Key) == 0))
                {
                    other.InRefs.Add(wrapper);
                }
                
                if (Array.Exists(other.Requires, x => x.CompareTo(wrapper.Key) == 0))
                {
                    wrapper.InRefs.Add(other);
                }
            }
        }
        
        private sealed class Wrapper
        {
            private readonly TopologicallySortedCollection<TKey, TItem> coll;

            private readonly TItem item;

            private readonly List<Wrapper> inRefs;

            private readonly List<Wrapper> inRefsForMarking;

            public Wrapper(TopologicallySortedCollection<TKey, TItem> coll, TItem item)
            {
                this.item = item;
                this.coll = coll;
                this.inRefs = new List<Wrapper>();
                this.inRefsForMarking = new List<Wrapper>();
            }
            
            public TKey Key
            {
                get { return this.coll.GetKey(this.Item); }
            }
            
            public TKey[] Requires
            {
                get { return this.coll.GetDependenciesSafe(this.Item); }
            }
            
            public TItem Item
            {
                get { return this.item; }
            }
            
            public List<Wrapper> InRefs
            {
                get { return this.inRefs; }
            }
            
            public List<Wrapper> InRefsForMarking
            {
                get { return this.inRefsForMarking; }
            }
        }
    }
}