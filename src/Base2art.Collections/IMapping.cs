﻿namespace Base2art.Collections
{
    public interface IMapping<in TKey, in TValue>
    {
        void Add(TKey key, TValue value);

        bool Remove(TKey key);

        void Clear();
    }
}
