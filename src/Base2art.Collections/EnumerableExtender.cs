﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Base2art.Collections;

    public static class EnumerableExtender
    {
        public static IEnumerable<T> GetEnumerable<T>(this T[] array)
        {
            return array;
        }

        public static IEnumerator<T> GetGenericEnumerator<T>(this T[] array)
        {
            return array.GetEnumerable().GetEnumerator();
        }

        public static IReadOnlyArrayList<T> AsReadOnlyArrayList<T>(this T[] array)
        {
            return new ReadOnlyArrayListWrapperForArray<T>(array);
        }

        public static IReadOnlyArrayList<T> AsReadOnlyArrayList<T>(this IList<T> array)
        {
            return new ReadOnlyArrayListWrapperForList<T>(array);
        }

        public static void ForAll<T>(this IEnumerable<T> collection, Action<T> action)
        {
            action.ValidateIsNotNull();
            collection.ValidateIsNotNull();
            
            // ReSharper disable PossibleMultipleEnumeration
            foreach (var item in collection)
            {
                action(item);
            }
            // ReSharper restore PossibleMultipleEnumeration
        }

        public static int IndexOf<T>(this IEnumerable<T> collection, T value)
            where T : IComparable<T>
        {
            return collection.IndexOf(arg => value.CompareTo(arg) == 0);
        }

        public static int IndexOf<T>(this IEnumerable<T> collection, Func<T, bool> func)
        {
            func.ValidateIsNotNull();
            collection.ValidateIsNotNull();
            
            // ReSharper disable PossibleMultipleEnumeration
            int i = 0;
            foreach (var item in collection)
            {
                if (func(item))
                {
                    return i;
                }

                i++;
            }

            throw new KeyNotFoundException("No Item Found in the collection");
            // ReSharper restore PossibleMultipleEnumeration
        }

        public static bool In<T>(this T item, IEnumerable<T> collection)
            where T : IEquatable<T>
        {
            return InInternal(item, collection);
        }

        public static bool In<T>(this T item, params T[] collection)
            where T : IEquatable<T>
        {
            return InInternal(item, collection);
        }

        private static bool InInternal<T>(T item, IEnumerable<T> collection) 
            where T : IEquatable<T>
        {
            collection.ValidateIsNotNull();
            
            return collection.Any(item.Equals);
        }
    }
}
