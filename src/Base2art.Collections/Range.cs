﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    public static class Range
    {
        public static IEnumerable<T> Create<T>(T stop)
            where T : struct, IComparable<T>
        {
            return Create<T>(Convert<T>(0), stop);
        }
        
        public static IEnumerable<T> Create<T>(T start, T stop)
            where T : struct, IComparable<T>
        {
            return Create<T>(start, stop, Convert<T>(1));
        }
        
        public static IEnumerable<T> Create<T>(T start, T stop, T step)
            where T : struct, IComparable<T>
        {
            return CreateInternal<T>(start, stop, step);
        }
        
        private static T Convert<T>(int i)
        {
            return (T)System.Convert.ChangeType(i, typeof(T), System.Globalization.CultureInfo.InvariantCulture);
        }
        
        // http://stackoverflow.com/questions/8122611/c-sharp-adding-two-generic-values
        private static IEnumerable<T> CreateInternal<T>(T start, T stop, T step)
            where T : IComparable<T>
        {
            // Declare the parameters
            var paramA = Expression.Parameter(typeof(T), "x");
            var paramB = Expression.Parameter(typeof(T), "y");

            // Add the parameters together
            BinaryExpression body = Expression.Add(paramA, paramB);

            // Compile it
            Func<T, T, T> add = Expression.Lambda<Func<T, T, T>>(body, paramA, paramB).Compile();

            return new RangeCollection<T>(start, stop, step, add);
        }
    }
}

/*
        public static IEnumerable<T> Create<T>(T stop, Func<T, T, T> add)
            where T : struct, IComparable<T>
        {
            return Create<T>(Convert<T>(0), stop, add);
        }
        
        public static IEnumerable<T> Create<T>(T start, T stop, Func<T, T, T> add)
            where T : struct, IComparable<T>
        {
            return Create<T>(start, stop, Convert<T>(1), add);
        }
        
        public static IEnumerable<T> Create<T>(T start, T stop, T step, Func<T, T, T> add)
            where T : struct, IComparable<T>
        {
            return new RangeCollection<T>(start, stop, step, add);
        }
        
 */