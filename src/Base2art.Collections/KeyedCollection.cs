﻿namespace Base2art.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class KeyedCollection<TKey, TValue> : IKeyedCollection<TKey, TValue>
    {
        private readonly CustomKeyedCollection items;

        public KeyedCollection(Func<TValue, TKey> keyExtractor)
        {
            this.items = new CustomKeyedCollection(keyExtractor);
        }

        public int Count
        {
            get
            {
                return this.items.Count;
            }
        }

        public IEnumerable<TKey> Keys
        {
            get
            {
                return this.items.Keys;
            }
        }

        public IEnumerable<TValue> Values
        {
            get
            {
                return this.items;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                return this.items[key];
            }
        }

        public IEnumerator<TValue> GetEnumerator()
        {
            return this.items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public bool Contains(TKey key)
        {
            return this.items.Contains(key);
        }

        public bool Contains(TValue value)
        {
            if (value == null)
            {
                return false;
            }

            TKey key = this.GetKeyForItem(value);
            return this.Contains(key);
        }

        public TKey GetKeyForItem(TValue value)
        {
            return this.items.GetKeyForItemInternal(value);
        }

        public void Add(TValue value)
        {
            this.items.Add(value);
        }

        public bool Remove(TKey key)
        {
            return this.items.Remove(key);
        }

        public bool Remove(TValue value)
        {
            if (value == null)
            {
                return false;
            }

            if (this.Contains(value))
            {
                return this.Remove(this.GetKeyForItem(value));
            }

            return false;
        }

        public void Clear()
        {
            this.items.Clear();
        }

        private sealed class CustomKeyedCollection
            : System.Collections.ObjectModel.KeyedCollection<TKey, TValue>
        {
            private readonly Func<TValue, TKey> keyExtractor;

            public CustomKeyedCollection(Func<TValue, TKey> keyExtractor)
            {
                this.keyExtractor = keyExtractor;
            }

            public IEnumerable<TKey> Keys
            {
                get
                {
                    return this.Dictionary.Keys;
                }
            }

            public TKey GetKeyForItemInternal(TValue value)
            {
                return this.GetKeyForItem(value);
            }

            protected override TKey GetKeyForItem(TValue item)
            {
                return this.keyExtractor(item);
            }
        }
    }
}
