﻿namespace Base2art.Collections
{
    using System.Collections.Generic;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public interface IReadOnlyMap<TKey, TValue> : IReadOnlyMapping<TKey, TValue>, IEnumerable<KeyValuePair<TKey, TValue>>
    {
        TValue this[TKey key] { get; }
    }
}