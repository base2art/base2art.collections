﻿namespace Base2art.Collections
{
    using System.Collections.Generic;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public interface IReadOnlyKeyedCollection<TKey, TValue>
        : IEnumerable<TValue>
    {
        int Count { get; }

        IEnumerable<TKey> Keys { get; }

        IEnumerable<TValue> Values { get; }
        
        TValue this[TKey key] { get; }

        bool Contains(TKey key);

        bool Contains(TValue value);

        TKey GetKeyForItem(TValue value);
    }
}