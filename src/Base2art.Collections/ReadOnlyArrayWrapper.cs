﻿namespace Base2art.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public class ReadOnlyArrayListWrapperForArray<T> : IReadOnlyArrayList<T>
    {
        private readonly T[] values;

        public ReadOnlyArrayListWrapperForArray(T[] values)
        {
            this.values = values;
        }

        public int Count
        {
            get
            {
                return this.values.Length;
            }
        }

        public T this[int index]
        {
            get
            {
                return this.values[index];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this.values.GetEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}