﻿namespace Base2art.Collections.Specialized
{
    using System;

    public static class PageExtender
    {
        public static int Start(this Page page)
        {
            page.ValidateIsNotNull();
            return page.PageIndex * page.PageSize;
        }
        
        public static int Start<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.Start();
        }
        
        public static int Start(this IPage page)
        {
            page.ValidateIsNotNull();
            return page.PageIndex * page.PageSize;
        }

        public static int End(this Page page)
        {
            page.ValidateIsNotNull();
            return ((page.PageIndex + 1) * page.PageSize) - 1;
        }

        public static int End<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.End();
        }

        public static int End(this IPage page)
        {
            page.ValidateIsNotNull();
            var pageSize = page.PageSize;
            var count = page.Count;
            if (count < pageSize)
            {
                return ((page.PageIndex * pageSize) - 1) + count;
            }
            
            return ((page.PageIndex + 1) * pageSize) - 1;
        }
        
        public static bool HasPreviousPage<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.HasPreviousPage();
        }

        public static bool HasPreviousPage(this IPage page)
        {
            page.ValidateIsNotNull();
            return page.PageIndex > 0;
        }

        public static bool HasPreviousPage(this Page page)
        {
            page.ValidateIsNotNull();
            return page.PageIndex > 0;
        }
        
        public static bool? HasNextPage<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.HasNextPage();
        }

        public static bool? HasNextPage(this IPage page)
        {
            page.ValidateIsNotNull();
            var totalCount = page.TotalCount;
            if (!totalCount.HasValue)
            {
                return null;
            }
            
            return (page.End() + 1) < totalCount.Value;
        }

        public static IPage PreviousPage<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.PreviousPage();
        }

        public static IPage PreviousPage(this IPage page)
        {
            page.ValidateIsNotNull();
            if (!page.HasPreviousPage())
            {
                throw new InvalidOperationException("Page Cannot be turned to a negative index");
            }
            
            return new PageWithCount(page.PageIndex - 1, page.PageSize, page.Count, page.TotalCount);
        }

        public static Page PreviousPage(this Page page)
        {
            page.ValidateIsNotNull();
            if (!page.HasPreviousPage())
            {
                throw new InvalidOperationException("Page Cannot be turned to a negative index");
            }
            
            return new Page(page.PageIndex - 1, page.PageSize);
        }
        
        public static IPage NextPage<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.NextPage();
        }

        public static IPage NextPage(this IPage page)
        {
            page.ValidateIsNotNull();
            var val = page.HasNextPage();
            if (val.HasValue && !val.Value)
            {
                throw new InvalidOperationException("Page Cannot be turned passed the end of results");
            }
            
            return new PageWithCount(page.PageIndex + 1, page.PageSize, page.Count, page.TotalCount);
        }
        
        public static IPage LastPage<T>(this IPagedCollection<T> page)
        {
            page.ValidateIsNotNull();
            return page.Page.LastPage();
        }

        public static IPage LastPage(this IPage page)
        {
            page.ValidateIsNotNull();
            
            if (page.HasNextPage() == null)
            {
                return null;
            }
            
            while (page.HasNextPage().GetValueOrDefault())
            {
                page = page.NextPage();
            }
            
            return page;
        }

        public static Page NextPage(this Page page)
        {
            page.ValidateIsNotNull();
            return new Page(page.PageIndex + 1, page.PageSize);
        }
        
        private sealed class PageWithCount : IPage
        {
            public PageWithCount(int pageIndex, int pageSize, int count, int? totalCount)
            {
                this.PageIndex = pageIndex;
                this.PageSize = pageSize;
                this.TotalCount = totalCount;
                this.Count = count;
            }
            
            public int PageIndex { get; set; }
            
            public int PageSize { get; set; }
            
            public int Count { get; set; }
            
            public int? TotalCount { get; set; }
        }
    }
}
