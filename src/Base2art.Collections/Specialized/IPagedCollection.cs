﻿namespace Base2art.Collections.Specialized
{
    public interface IPagedCollection<out T> : IReadOnlyArrayList<T>
    {
        IPage Page { get; }

        int? TotalCount { get; }
    }   
}