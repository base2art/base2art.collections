﻿namespace Base2art.Collections.Specialized
{
    using System.Collections.Generic;
    using System.Linq;
    
    public class PagedCollection<T> : IPagedCollection<T>, IPage
    {
        private readonly int? totalCount;
        
        private readonly T[] items;

        private readonly Page page;

        public PagedCollection(IEnumerable<T> items, Page page)
            : this(items, page, null)
        {
        }
        
        public PagedCollection(IEnumerable<T> items, Page page, int? totalCount)
        {
            this.page = page;
            this.items = items.ToArray();
            this.totalCount = totalCount;
        }
        
        public int Count
        {
            get { return this.items.Length; }
        }
        
        public int? TotalCount
        {
            get
            {
                return this.totalCount;
            }
        }
        
        public IPage Page
        {
            get { return this; }
        }

        public int PageIndex
        {
            get { return this.page.PageIndex; }
        }

        public int PageSize
        {
            get { return this.page.PageSize; }
        }

        public T this[int index]
        {
            get { return this.items[index]; }
        }
        
        public IEnumerator<T> GetEnumerator()
        {
            return this.items.GetGenericEnumerator();
        }
        
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
