﻿namespace Base2art.Collections.Specialized
{
    public class Page
    {
        private int index;

        private int size;

        public Page()
        {
        }

        public Page(int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
        }

        public int PageIndex
        {
            get
            {
                if (this.index < 0)
                {
                    return 0;
                }
                
                return this.index;
            }
            
            set
            {
                this.index = value;
            }
        }

        public int PageSize
        {
            get
            {
                return this.size <= 0 ? 10 : this.size;
            }
            
            set
            {
                this.size = value;
            }
        }
    }
}
