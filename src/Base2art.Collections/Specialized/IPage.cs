﻿namespace Base2art.Collections.Specialized
{
    public interface IPage
    {
        int PageIndex
        {
            get;
        }

        int PageSize
        {
            get;
        }

        int? TotalCount
        {
            get;
        }

        int Count
        {
            get;
        }
    }
}
