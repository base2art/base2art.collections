﻿namespace Base2art.Collections
{
    using System.Collections.Generic;

    public interface IReadOnlyMapping<TKey, out TValue>
    {
        int Count { get; }

        IEnumerable<TKey> Keys { get; }

        IEnumerable<TValue> Values { get; }

        bool Contains(TKey key);
    }
}
