﻿namespace Base2art.Collections
{
    using System.Collections.Generic;
    using System.Linq;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Multi", Justification = "SjY")]
    public interface IReadOnlyMultiMap<TKey, out TValue> : IReadOnlyMapping<TKey, TValue>, IEnumerable<IGrouping<TKey, TValue>>
    {
        IEnumerable<TValue> this[TKey key] { get; }
    }
}