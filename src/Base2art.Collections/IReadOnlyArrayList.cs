﻿namespace Base2art.Collections
{
    using System.Collections.Generic;
    
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public interface IReadOnlyArrayList<out T> : IEnumerable<T>
    {
        int Count { get; }

        T this[int index] { get; }
    }
}