﻿namespace Base2art.Collections
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public interface IKeyedCollection<TKey, TValue>
        : IReadOnlyKeyedCollection<TKey, TValue>
    {
        void Add(TValue value);

        bool Remove(TKey key);

        bool Remove(TValue value);

        void Clear();
    }
}