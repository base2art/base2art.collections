﻿namespace Base2art.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    [Serializable]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Multi", Justification = "SjY")]
    public class MultiMap<TKey, TValue> : IMultiMap<TKey, TValue>
    {
        private readonly Dictionary<TKey, List<TValue>> dict
            = new Dictionary<TKey, List<TValue>>();

        public int Count
        {
            get
            {
                return this.dict.Count;
            }
        }

        public IEnumerable<TKey> Keys
        {
            get
            {
                return this.dict.Keys;
            }
        }

        public IEnumerable<TValue> Values
        {
            get
            {
                return this.Keys.SelectMany(key => this.dict[key]);
            }
        }

        public IEnumerable<TValue> this[TKey key]
        {
            get
            {
                return this.dict[key];
            }

            set
            {
                if (this.dict.ContainsKey(key))
                {
                    var values = this.dict[key];
                    values.Clear();
                }

                value.ForAll(x => this.Add(key, x));
            }
        }

        public IEnumerator<IGrouping<TKey, TValue>> GetEnumerator()
        {
            foreach (var key in this.Keys)
            {
                yield return new Grouping(key, this.dict[key]);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public bool Contains(TKey key)
        {
            return this.dict.ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            if (!this.dict.ContainsKey(key))
            {
                this.dict[key] = new List<TValue>();
            }

            this.dict[key].Add(value);
        }

        public bool Remove(TKey key)
        {
            if (this.dict.ContainsKey(key))
            {
                this.dict.Remove(key);
                return true;
            }

            return false;
        }

        public void Clear()
        {
            this.dict.Clear();
        }

        private class Grouping : IGrouping<TKey, TValue>
        {
            private readonly TKey key;

            private readonly List<TValue> values;

            public Grouping(TKey key, List<TValue> values)
            {
                this.key = key;
                this.values = values;
            }

            public TKey Key
            {
                get
                {
                    return this.key;
                }
            }

            public IEnumerator<TValue> GetEnumerator()
            {
                return this.values.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
        }
    }
}