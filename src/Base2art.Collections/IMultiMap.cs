﻿namespace Base2art.Collections
{
    using System.Collections.Generic;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Multi", Justification = "SjY")]
    public interface IMultiMap<TKey, TValue> : IReadOnlyMultiMap<TKey, TValue>, IMapping<TKey, TValue>
    {
        new IEnumerable<TValue> this[TKey key] { get; set; }
    }
}
