﻿namespace Base2art.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    [Serializable]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public class Map<TKey, TValue> : IMap<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> backingDict = new Dictionary<TKey, TValue>();

        public int Count
        {
            get
            {
                return this.backingDict.Count;
            }
        }

        public IEnumerable<TKey> Keys
        {
            get
            {
                return this.backingDict.Keys;
            }
        }

        public IEnumerable<TValue> Values
        {
            get
            {
                return this.backingDict.Values;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                return this.backingDict[key];
            }

            set
            {
                this.backingDict[key] = value;
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this.backingDict.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public bool Contains(TKey key)
        {
            return this.backingDict.ContainsKey(key);
        }

        public void Add(TKey key, TValue value)
        {
            this.backingDict.Add(key, value);
        }

        public bool Remove(TKey key)
        {
            return this.backingDict.Remove(key);
        }

        public void Clear()
        {
            this.backingDict.Clear();
        }
    }
}