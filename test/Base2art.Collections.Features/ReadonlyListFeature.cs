﻿namespace Base2art.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ReadonlyListFeature
    {
        [Test]
        public void ShouldHaveArrayApi()
        {
            var arr = (new[] { "a", "b" }).AsReadOnlyArrayList();
            string item = arr[1];
            item.Should().Be("b");
            arr.Count.Should().Be(2);

            arr.ToList().Count.Should().Be(2);
            ((IEnumerable)arr).OfType<string>().ToList().Count.Should().Be(2);
        }

        [Test]
        public void ShouldHaveListApi()
        {
            var values = new List<string>() { "a", "b" };
            IReadOnlyArrayList<string> arr = values.AsReadOnlyArrayList();
            string item = arr[1];
            item.Should().Be("b");
            arr.Count.Should().Be(2);

            arr.ToList().Count.Should().Be(2);
            ((IEnumerable)arr).OfType<string>().ToList().Count.Should().Be(2);
        }
    }
}
