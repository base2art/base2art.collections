﻿namespace Base2art.Collections.Specialized
{
	using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.Collections.Specialized;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class PagedCollectionFeature
    {
        [Test]
        public void ShouldVerifyPageInfo()
        {
            Page p = new Page();
            p.PageIndex.Should().Be(0);
            p.PageSize.Should().Be(10);
            p.Start().Should().Be(0);
            p.End().Should().Be(9);
            
            p = new Page(2, 40);
            p.PageIndex.Should().Be(2);
            p.PageSize.Should().Be(40);
            p.Start().Should().Be(80);
            p.End().Should().Be(119);
        }
        
        [Test]
        public void ShouldVerifyPageIndexLessThanZero()
        {
            Page p = new Page();
            p.PageIndex.Should().Be(0);
            p.PageSize.Should().Be(10);
            p.Start().Should().Be(0);
            p.End().Should().Be(9);
            
            p.PageIndex = -1;
            p.PageIndex.Should().Be(0);
        }
        
        [Test]
        public void ShouldVerifyPaging()
        {
            var items = Create(30);
            
            Page p = new Page();
            var pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p);
            pc.Count.Should().Be(10);
            
            for (int i = 0; i < 10; i++)
            {
                pc[i].Should().Be(i.ToString());
            }
            
            pc.TotalCount.HasValue.Should().BeFalse();
            pc.TotalCount.Should().Be(null);
            
            var arr = pc.Select(x => x).ToArray();
            arr.Length.Should().Be(10);
            
            for (int i = 0; i < 10; i++)
            {
                arr[i].Should().Be(i.ToString());
            }
            
            //.OfType<string>()
            var arr1 = ((System.Collections.IEnumerable)pc).OfType<string>().Select(x => x).ToArray();
            arr1.Length.Should().Be(10);
            
            for (int i = 0; i < 10; i++)
            {
                arr1[i].Should().Be(i.ToString());
            }
        }
        
        [Test]
        public void ShouldVerifyPagingWithTotalCount()
        {
            var items = Create(30);
            
            Page p = new Page();
            var pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
            pc.Count.Should().Be(10);
            
            for (int i = 0; i < 10; i++)
            {
                pc[i].Should().Be(i.ToString());
            }
            
            pc.TotalCount.HasValue.Should().BeTrue();
            pc.TotalCount.Should().Be(30);
            
            var arr = pc.Select(x => x).ToArray();
            arr.Length.Should().Be(10);
            
            for (int i = 0; i < 10; i++)
            {
                arr[i].Should().Be(i.ToString());
            }
            
            //.OfType<string>()
            var arr1 = ((System.Collections.IEnumerable)pc).OfType<string>().Select(x => x).ToArray();
            arr1.Length.Should().Be(10);
            
            for (int i = 0; i < 10; i++)
            {
                arr1[i].Should().Be(i.ToString());
            }
        }
        
        [Test]
        public void ShouldHaveExtensionMethods()
        {
            var items = Create(30);
            
            {
                Page p = new Page(2, 4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
                pc.Start().Should().Be(8);
                pc.End().Should().Be(11);
                pc.Page.PageIndex.Should().Be(2);
                pc.Page.PageSize.Should().Be(4);
                pc.HasPreviousPage().Should().Be(true);
                pc.Page.LastPage().PageIndex.Should().Be(7);
                pc.LastPage().PageIndex.Should().Be(7);
            }
            
            {
                Page p = new Page(0, 4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
            
                pc.HasPreviousPage().Should().Be(false);
                pc.HasNextPage().Should().Be(true);
                pc.Page.LastPage().PageIndex.Should().Be(7);
                pc.LastPage().PageIndex.Should().Be(7);
            }
        }
        
        [Test]
        public void ShouldHaveNextPage()
        {
            var items = Create(8);
            
            {
                Page p = new Page(0, 4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p);
            
                pc.HasNextPage().HasValue.Should().BeFalse();
            }
            
            {
                Page p = new Page(0, 4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
            
                pc.HasNextPage().HasValue.Should().BeTrue();
                pc.HasNextPage().Should().BeTrue();
            }
            
            {
                Page p = new Page(1, 4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
            
                pc.HasNextPage().HasValue.Should().BeTrue();
                pc.HasNextPage().Should().BeFalse();
            }
        }
        
        [Test]
        public void ShouldGetNextAndPreviousPage()
        {
            var items = Create(8);
            {
                Page p = new Page(0, 4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p);
                pc.NextPage().PageIndex.Should().Be(1);
                pc.NextPage().PageSize.Should().Be(4);
                pc.NextPage().NextPage().PageIndex.Should().Be(2);
                pc.NextPage().NextPage().PageSize.Should().Be(4);
            
                pc.NextPage().NextPage().PreviousPage().PageIndex.Should().Be(1);
                pc.NextPage().NextPage().PreviousPage().PageSize.Should().Be(4);
            
                pc.NextPage().NextPage().PreviousPage().PreviousPage().PageIndex.Should().Be(0);
                pc.NextPage().NextPage().PreviousPage().PreviousPage().PageSize.Should().Be(4);
            }
            
            {
                Page p = new Page(2, 4);
                p.PreviousPage().PageIndex.Should().Be(1);
                p.PreviousPage().PageSize.Should().Be(4);
                IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p);
                pc.PreviousPage().PageIndex.Should().Be(1);
                pc.PreviousPage().PageSize.Should().Be(4);
                pc.LastPage().Should().BeNull();
            }
        }
        
        [Test]
        public void ShouldGetPreviousPageThrowsExcetionsAcrossBoundries()
        {
            var items = Create(10);
            Page p = new Page(0, 5);
            new Action(() => p.PreviousPage()).ShouldThrow<InvalidOperationException>();
            IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p);
            
            new Action(() => pc.PreviousPage()).ShouldThrow<InvalidOperationException>();
            
        }
        
        [Test]
        public void ShouldGetNextPageThrowsExcetionsAcrossBoundriesNoExceptionWhenNonTotalCount()
        {
            var items = Create(10);
            Page p = new Page(0, 5);
            p.NextPage().NextPage().NextPage().NextPage().NextPage().NextPage().NextPage().NextPage().NextPage();
            new Action(() => p.PreviousPage()).ShouldThrow<InvalidOperationException>();
            IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p);
            pc.NextPage().NextPage().NextPage().NextPage().NextPage().NextPage().NextPage().NextPage().NextPage();
        }
        
        [Test]
        public void ShouldGetNextPageThrowsExcetionsAcrossBoundriesExceptionWhenTotalCount()
        {
            var items = Create(10);
            Page p = new Page(0, 5);
            new Action(() => p.PreviousPage()).ShouldThrow<InvalidOperationException>();
            IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
            pc.NextPage();
            new Action(() => pc.NextPage().NextPage()).ShouldThrow<InvalidOperationException>();
        }
        
        [Test]
        public void ShouldGetCorrectEnd()
        {
            var items = Create(10);
            Page p = new Page(0, 20);
            p.End().Should().Be(19);
            IPagedCollection<string> pc = new PagedCollection<string>(items.Skip(p.Start()).Take(p.PageSize), p, items.Count);
            pc.End().Should().Be(9);
        }

        private static List<string> Create(int count)
        {
            var items = new List<string>();
            for (int i = 0; i < count; i++)
            {
                items.Add(i.ToString());
            }
			
            return items;
        }
    }
}
/*
 
 
 
 */

    
//    public interface IPaginationInformation
//    {
//
//        int? TotalCount { get; }
//
//        bool HasPreviousPage { get; }
//
//        bool? HasNextPage { get; }
//
//        IPaginationInformation NextPage { get; }
//
//        IPaginationInformation PreviousPage { get; }
//
//        Page Page { get; }
//    }
