﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class MultiMapFeature
    {
        [Test]
        public void ShouldHaveApi()
        {
            IMultiMap<string, string> map = new MultiMap<string, string>();
            map.Add("a", "z");
            map["a"].Should().BeEquivalentTo("z");
            map.Keys.Should().BeEquivalentTo(new[] { "a" });
            map.Values.Should().BeEquivalentTo(new[] { "z" });
            new Action(() => map.Add("a", map["b"].FirstOrDefault())).ShouldThrow<KeyNotFoundException>();

            map.Count.Should().Be(1);

            map.Clear();

            map.Count.Should().Be(0);
            new Action(() => map["a"].Should().BeEquivalentTo("z")).ShouldThrow<KeyNotFoundException>();

            map["a"] = new[] { "z" };
            map["a"] = new[] { "y" };
            map["a"].Should().BeEquivalentTo("y");
            map.Contains("a").Should().BeTrue();
            map.Contains("b").Should().BeFalse();
            map.Keys.Should().BeEquivalentTo(new[] { "a" });
            map.Values.Should().BeEquivalentTo(new[] { "y" });
            map.Count.Should().Be(1);

            {
                var enumer = map.GetEnumerator();
                enumer.MoveNext().Should().BeTrue();
                var grouping = enumer.Current;
                grouping.Key.Should().Be("a");
                grouping.AsEnumerable().Should().BeEquivalentTo("y");
                enumer.MoveNext().Should().BeFalse();
            }

            {
                
                var enumer = ((System.Collections.IEnumerable)map).GetEnumerator();
                enumer.MoveNext().Should().BeTrue();
                IGrouping<string, string> grouping = (IGrouping<string, string>)enumer.Current;
                grouping.Key.Should().Be("a");
                grouping.AsEnumerable().Should().BeEquivalentTo("y");
                
                ((System.Collections.IEnumerable)grouping).OfType<string>().Select(x => x).Should().BeEquivalentTo("y");
                
                enumer.MoveNext().Should().BeFalse();
            }
            {
                List<IGrouping<string, string>> items = new List<IGrouping<string, string>>();
                foreach (object element in map)
                {
                    items.Add((IGrouping<string, string>)element);
                }
                
                var enumer = items.GetEnumerator();
                enumer.MoveNext().Should().BeTrue();
                IGrouping<string, string> grouping = (IGrouping<string, string>)enumer.Current;
                grouping.Key.Should().Be("a");
                grouping.AsEnumerable().Should().BeEquivalentTo("y");
                enumer.MoveNext().Should().BeFalse();
            }

            map.Remove("a").Should().BeTrue();
            map.Remove("b").Should().BeFalse();
        }
    }
}
