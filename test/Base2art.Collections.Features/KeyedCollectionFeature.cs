﻿namespace Base2art.Collections
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class KeyedCollectionFeature
    {
        [Test]
        public void ShouldSaveAndLoadItems()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            coll.Count.Should().Be(7);
            IEnumerable coll1 = this.CreateCollection(7);
            int i = 0;
            IEnumerator enumerator = coll1.GetEnumerator();
            while (enumerator.MoveNext())
            {
                i++;
            }

            i.Should().Be(7);
        }

        [Test]
        public void ShouldSaveAndLoadItemsGeneric()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            coll.Count.Should().Be(7);
            int i = 0;
            IEnumerator<Person> enumerator = coll.GetEnumerator();
            while (enumerator.MoveNext())
            {
                i++;
            }

            i.Should().Be(7);
        }

        [Test]
        public void ShouldClearItems()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            coll.Should().HaveCount(7);
            coll.Clear();
            coll.Should().HaveCount(0);
        }

        [Test]
        public void ShouldContainKey()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            coll.Contains(3).Should().BeTrue();
            coll.Contains(8).Should().BeFalse();
            coll.Clear();
            coll.Should().HaveCount(0);
            coll.Contains(3).Should().BeFalse();
        }

        [Test]
        public void ShouldContainItem()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            coll.Contains(new Person { Id = 3 }).Should().BeTrue();
            coll.Contains(new Person { Id = 8 }).Should().BeFalse();
            coll.Clear();
            coll.Should().HaveCount(0);
            coll.Contains(new Person { Id = 3 }).Should().BeFalse();
            coll.Contains(null).Should().BeFalse();
        }

        [Test]
        public void ShouldHaveTheRightKeys()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            var keys = coll.Keys.ToArray();
            keys.Should().Equal(new[] { 1, 2, 3, 4, 5, 6, 7 });
        }

        [Test]
        public void ShouldHaveTheRightValues()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            var keys = coll.Values.Select(x => x.Id).ToArray();
            keys.Should().Equal(new[] { 1, 2, 3, 4, 5, 6, 7 });
        }

        [Test]
        public void ShouldRemoveItems()
        {
            IKeyedCollection<int, Person> coll = this.CreateCollection(7);
            coll.Remove(coll[1]).Should().BeTrue();
            coll.Count.Should().Be(6);
            coll.Remove(2).Should().BeTrue();
            coll.Count.Should().Be(5);

            coll.Remove(2).Should().BeFalse();
            coll.Count.Should().Be(5);

            coll.Remove(new Person { Id = 3 }).Should().BeTrue();
            coll.Count.Should().Be(4);

            coll.Remove(new Person { Id = 3 }).Should().BeFalse();
            coll.Count.Should().Be(4);

            coll.Remove(null).Should().BeFalse();
            coll.Count.Should().Be(4);
        }

        private IKeyedCollection<int, Person> CreateCollection(int count)
        {
            IKeyedCollection<int, Person> coll = new KeyedCollection<int, Person>(x => x.Id);
            for (int i = 0; i < count; i++)
            {
                coll.Add(new Person { Id = i + 1, Name = "My Name " + 1 });
            }

            return coll;
        }

        private class Person
        {
            public int Id { get; set; }

            public string Name { get; set; }
        }
    }
}
