﻿namespace Base2art.Collections
{
    using System;
    using System.Linq;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class RangeFeature
    {
        [Test]
        public void NullRef_Checking()
        {
            Action mut;
            mut = () => new RangeCollection<int>(1, 2, 3, null);
            mut.ShouldThrow<ArgumentNullException>();
            
            mut = () => new RangeCollection<int>(1, 2, 0, (x, y) => x + y);
            mut.ShouldThrow<ArgumentOutOfRangeException>();
        }
        
        [Test]
        public void ShouldProcessGenericRange()
        {
            var range = Range.Create(0L, 4L, 1L);
            var item = range.ToArray();
            item.Length.Should().Be(4);
            item[0].Should().Be(0);
            item[1].Should().Be(1);
            item[2].Should().Be(2);
            item[3].Should().Be(3);
        }
        
        [Test]
        public void ShouldProcessGenericRange_Decimal()
        {
            var range = Range.Create(0.1, 6.3, .5);
            var item = range.ToArray();
            item.Length.Should().Be(13);
            item[0].Should().Be(0.1);
            item[1].Should().Be(0.6);
            item[2].Should().Be(1.1);
            item[3].Should().Be(1.6);
            item.Last().Should().Be(6.1);
        }
        
        [Test]
        public void ShouldLoadStandard()
        {
            var range = Range.Create(0, 4, 1);
            var item = range.ToArray();
            item.Length.Should().Be(4);
            item[0].Should().Be(0);
            item[1].Should().Be(1);
            item[2].Should().Be(2);
            item[3].Should().Be(3);
        }
        
        [Test]
        public void ShouldPreventInfiniteLoop()
        {
            var range = Range.Create(0, -2, 1);
            var item = range.ToArray();
            item.Length.Should().Be(0);
        }
        
        [Test]
        public void ShouldRunBackward()
        {
            var range = Range.Create(0, -4, -1);
            var item = range.ToArray();
            item.Length.Should().Be(4);
            item[0].Should().Be(0);
            item[1].Should().Be(-1);
            item[2].Should().Be(-2);
            item[3].Should().Be(-3);
        }
        
        [Test]
        public void ShouldRunStep()
        {
            {
                var range = Range.Create(1, 5, 2);
                var item = range.ToArray();
                item.Length.Should().Be(2);
                item[0].Should().Be(1);
                item[1].Should().Be(3);
            }
            
            {
                var range = Range.Create(1, -5, -2);
                var item = range.ToArray();
                item.Length.Should().Be(3);
                item[0].Should().Be(1);
                item[1].Should().Be(-1);
                item[2].Should().Be(-3);
            }
            
            {
                var range = Range.Create(-1, -5, -2);
                var item = range.ToArray();
                item.Length.Should().Be(2);
                item[0].Should().Be(-1);
                item[1].Should().Be(-3);
            }
            
            {
                var range = Range.Create(0, 5, 2);
                var item = range.ToArray();
                item.Length.Should().Be(3);
                item[0].Should().Be(0);
                item[1].Should().Be(2);
                item[2].Should().Be(4);
            }
            
            {
                var range = Range.Create(0, -5, -2);
                var item = range.ToArray();
                item.Length.Should().Be(3);
                item[0].Should().Be(0);
                item[1].Should().Be(-2);
                item[2].Should().Be(-4);
            }
        }
        
        [Test]
        public void ShouldHaveCtorOverloads()
        {
            {
                var range = Range.Create(4);
                var item = range.ToArray();
                item.Length.Should().Be(4);
                item[0].Should().Be(0);
                item[1].Should().Be(1);
                item[2].Should().Be(2);
                item[3].Should().Be(3);
            }
            
            {
                var range = Range.Create(1, 4);
                var item = range.ToArray();
                item.Length.Should().Be(3);
                item[0].Should().Be(1);
                item[1].Should().Be(2);
                item[2].Should().Be(3);
            }
        }
        
        [Test]
        public void ShouldHaveEnumerOverload()
        {
            {
                var item = ((System.Collections.IEnumerable)Range.Create(4)).OfType<int>().ToArray();
                item.Length.Should().Be(4);
                item[0].Should().Be(0);
                item[1].Should().Be(1);
                item[2].Should().Be(2);
                item[3].Should().Be(3);
            }
        }
    }
}
