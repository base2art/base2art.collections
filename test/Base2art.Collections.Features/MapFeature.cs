﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class MapFeature
    {
        [Test]
        public void ShouldHaveApi()
        {
            IMap<string, string> map = new Map<string, string>();
            map.Add("a", "z");
            map["a"].Should().Be("z");
            map.Keys.Should().BeEquivalentTo(new[] { "a" });
            map.Values.Should().BeEquivalentTo(new[] { "z" });
            new Action(() => map.Add("a", map["b"])).ShouldThrow<KeyNotFoundException>();

            map.Count.Should().Be(1);

            map.Clear();

            map.Count.Should().Be(0);
            new Action(() => map["a"].Should().Be("z")).ShouldThrow<KeyNotFoundException>();

            map["a"] = "z";
            map["a"] = "y";
            map["a"].Should().Be("y");
            map.Contains("a").Should().BeTrue();
            map.Contains("b").Should().BeFalse();
            map.Keys.Should().BeEquivalentTo(new[] { "a" });
            map.Values.Should().BeEquivalentTo(new[] { "y" });
            map.Count.Should().Be(1);

            {

                var enumer = map.GetEnumerator();
                enumer.MoveNext().Should().BeTrue();
                enumer.Current.Should().Be(new KeyValuePair<string, string>("a", "y"));
                enumer.MoveNext().Should().BeFalse();
            }
         
            {
                var enumer = ((System.Collections.IEnumerable)map).GetEnumerator();
                enumer.MoveNext().Should().BeTrue();
                enumer.Current.Should().Be(new KeyValuePair<string, string>("a", "y"));
                enumer.MoveNext().Should().BeFalse();
            }

            map.Remove("a").Should().BeTrue();
            map.Remove("b").Should().BeFalse();
        }
    }
}
