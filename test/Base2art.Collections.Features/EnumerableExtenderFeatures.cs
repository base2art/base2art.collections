﻿namespace Base2art.Collections
{
    using System;
    using System.Collections.Generic;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class EnumerableExtenderFeatures
    {
        [Test]
        public void ShouldGetArrayEnumerator()
        {
            var coll = new[] { "a", "b", "c" };
            var c1 = coll.GetEnumerable();
            c1.Should().Equal(coll);
        }

        [Test]
        public void ShouldGetGenericEnumerator()
        {
            var coll = new[] { "a", "b", "c" };
            var list = new List<string>();
            var c1 = coll.GetGenericEnumerator();
            while (c1.MoveNext())
            {
                list.Add(c1.Current);
            }
        }

        [Test]
        public void ShouldDoForAll()
        {
            var coll = new[] { "a", "b", "c" };
            var i = 0;
            var cat = string.Empty;
            coll.ForAll(x => i += 1);
            coll.ForAll(x => cat += x);
            i.Should().Be(3);
            cat.Should().Be("abc");
        }

        [Test]
        public void ShouldDoIndexOfEnumerable()
        {
            var coll = new[] { "a", "b", "c" };
            coll.IndexOf("a").Should().Be(0);
            coll.IndexOf("b").Should().Be(1);
            coll.IndexOf("c").Should().Be(2);
            new Action(() => coll.IndexOf("d")).ShouldThrow<KeyNotFoundException>();
        }

        [Test]
        public void ShouldDoIndexOfArray()
        {
            "a".In((IEnumerable<string>)new[] { "a", "b", "c" }).Should().BeTrue();
            "d".In((IEnumerable<string>)new[] { "a", "b", "c" }).Should().BeFalse();

            "a".In("a", "b", "c").Should().BeTrue();
            "d".In("a", "b", "c").Should().BeFalse();
        }
    }
}
